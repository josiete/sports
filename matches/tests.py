import pytz
from datetime import datetime
from django.test import TestCase
from entities.models import Location, Team, Player
from .models import Match, Goal, Card


class MatchTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        location1 = Location.objects.create(town="Alfarobeira",
                                            county="Algarve")
        location1.save()
        location2 = Location.objects.create(town="Braga", county="Braga")
        location2.save()
        cls.team1 = Team.objects.create(name="Algarve FC", location=location1)
        cls.team1.save()
        cls.team2 = Team.objects.create(name="Braga CF", location=location2)
        cls.match = Match.objects.create(
            local_team=cls.team1,
            visiting_team=cls.team2,
            location=cls.team1.location,
            start_date=datetime(2020, 1, 1, 12, 0, tzinfo=pytz.UTC),
        )
        cls.match.save()
        cls.player1 = Player(
            name='Lucas',
            surname='Vazquez',
            nickname='Lucas Vazquez',
            number=9,
            team=cls.team1,
        )
        cls.player1.save()
        cls.player2 = Player(
            name='Manuel',
            surname='Barroso',
            nickname='Chiquito',
            number=3,
            team=cls.team2,
        )
        cls.player2.save()

    def test_match_status(self):
        self.match.start()
        self.assertEquals(self.match.status, 'P')
        self.match.end()
        self.assertEquals(self.match.status, 'E')

    def test_match_events(self):
        self.match.start()
        goal_event1 = Goal(minute=30, local_score=1, visit_score=0,
                           player=self.player1, match=self.match)
        goal_event1.save()
        import ipdb; ipdb.set_trace()
 #       self.match.add_event(goal_event1)
 #       card_event1 = Card(37, self.player1, 'yellow')
 #       self.match.add_event(card_event1)
 #       goal_event2 = Goal(45, 1, 1, self.player2)
 #       self.match.add_event(goal_event2)
 #       goal_event3 = Goal(70, 2, 1, self.player1)
 #       self.match.add_event(goal_event3)

 #       self.assertEquals(len(self.match.events), 4)
 #       self.assertEquals(self.match.current_score, (2, 1))

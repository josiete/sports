from django.contrib import admin
from .models import (Match, Event,
                     Team, Player,
                     LineUp)

admin.site.register([Match, Event, Team, Player, LineUp])

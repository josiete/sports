from djongo import models
from django import forms


class Team(models.Model):
    team_name = models.CharField(max_length=128)

    def __str__(self):
        return self.team_name

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('team_name',)


class Player(models.Model):
    name = models.CharField(max_length=128)
    surname = models.CharField(max_length=128)
    nickname = models.CharField(max_length=128)
    number = models.IntegerField()
    team = models.EmbeddedField(
        model_container=Team,
        model_form_class=TeamForm,
    )

    def __str__(self):
        return "{} {} ({})".format(self.name, self.surname, self.nickname)


class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = ('name', 'surname', 'nickname', 'number')


class Match(models.Model):
    local_team_name = models.CharField(max_length=128)
    visit_team_name = models.CharField(max_length=128)
    location_town_name = models.CharField(max_length=128)
    start_date = models.DateField()

    def __str__(self):
        return "{} vs {}".format(self.local_team_name, self.visit_team_name)


class MatchForm(forms.ModelForm):
    class Meta:
        model = Match
        fields = ('local_team_name', 'visit_team_name', 'location_town_name',
                  'start_date')


class LineUp(models.Model):
    match = models.EmbeddedField(
        model_container=Match,
        model_form_class=MatchForm,
    )
    local_players = models.ArrayField(
        model_container=Player,
        model_form_class=PlayerForm,
    )


class Event(models.Model):

    minute = models.TimeField()
    event_type = models.CharField(max_length=128)
    match = models.EmbeddedField(
        model_container=Match,
        model_form_class=MatchForm,
    )
    objects = models.DjongoManager()


Para levantar los contenedores usa:

$ docker-compose up

Puedes correr los test de Django entrando en el contenedor de Django mediante 

$ docker exec -i -t ampere_django_1 /bin/bash

y despues, dentro de la consola, mediante ...

$ cd /code
$ python manage.py test

Una vez levantados los contenedores se puede acceder al panel de administracion mediante 

http://localhost:8000/admin

user: admin
pass: 1234



-----

He usado Djongo (https://nesdis.github.io/djongo/) como conector para MongoDB. 
 





